package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment implements AdapterView.OnItemSelectedListener  {

    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
         DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
         poste = "";
         salle = DISTANCIEL;
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spPoste = (Spinner) view.findViewById(R.id.spPoste);
        Spinner spSalle = (Spinner) view.findViewById(R.id.spSalle);

        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(view.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(view.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);

        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterPoste);
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapterSalle);

        spPoste.setOnItemSelectedListener(this);
        spPoste.setOnItemSelectedListener(this);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView tvlogin = (TextView) getActivity().findViewById(R.id.tvLogin);
            model.setUsername(tvlogin.getText().toString());
            Toast.makeText(getContext(), "Login : " + tvlogin.getText().toString(), Toast.LENGTH_LONG).show();
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);

        });

        // TODO Q5.b
        update();
        // TODO Q9
    }

    // TODO Q5.a
    public  void update(){
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        if(this.DISTANCIEL.equals(this.salle)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(this.salle+" : "+this.poste);
        }



    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        switch (parent.getId()) {
            case R.id.spPoste:
                poste = parent.getSelectedItem().toString();
                Toast.makeText(getContext(), "Poste : " + poste, Toast.LENGTH_LONG).show();
                break;
            case R.id.spSalle:
                salle = parent.getSelectedItem().toString();
                Toast.makeText(getContext(), "Salle : " + salle, Toast.LENGTH_LONG).show();
                break;
        }
        update();
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    // TODO Q9
}